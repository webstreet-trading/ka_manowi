<?php

class WS_Builder_Module_Blog extends ET_Builder_Module_Blog {

    /**
     * The string used to uniquely identify
     * 
     * @var string 
     */
    public $textdomain = null;

    /**
     * Init module
     */
    function init() {
        parent::init();

        $this->textdomain = WS::$textdomain;

        $this->name = esc_html__('Blog Post Type', $this->textdomain);
        $this->slug = 'ws_et_pb_blog';

        $this->whitelisted_fields[] = 'post_type';
        $this->whitelisted_fields[] = 'taxonomy';
        $this->whitelisted_fields[] = 'include_taxonomies';
        $this->whitelisted_fields[] = 'template';

        $this->fields_defaults['post_type'] = array('post', 'add_default_setting');
        $this->fields_defaults['template'] = array('default.php');
    }

    /**
     * Get module fields
     */
    function get_fields() {
        $fields = parent::get_fields();

        // get post types
        $post_type_objects = get_post_types(array('public' => true), 'objects');
        $post_types = array();
        foreach ($post_type_objects as $post_type => $post_type_object) {
            $post_types[$post_type] = $post_type_object->labels->name;
        }

        // get templates
        $available_templates = WS::directory_files(dirname(WS::get_viewpath('modules/blog/default.php')));
        $templates = array_combine($available_templates, $available_templates);

        // add post type and taxononmy fields
        $fields = array_merge(array(
            'post_type' => array(
                'label' => esc_html__('Post Type', $this->textdomain),
                'type' => 'select',
                'option_category' => 'layout',
                'options' => $post_types,
                'toggle_slug' => 'main_content',
                'computed_affects' => array(
                    '__posts',
                ),
            ),
            'taxonomy' => array(
                'label' => esc_html__('Taxonomy slug', $this->textdomain),
                'description' => esc_html__('Taxononmy to use for below include.', $this->textdomain),
                'type' => 'text',
                'option_category' => 'configuration',
                'computed_affects' => array(
                    '__posts',
                ),
                'toggle_slug' => 'main_content',
            ),
            'include_taxonomies' => array(
                'label' => esc_html__('Include Taxonomy slug(s)', $this->textdomain),
                'description' => esc_html__('Comma delimited list of taxonomy slug(s) to select. Leave empty to include all.', $this->textdomain),
                'type' => 'text',
                'option_category' => 'configuration',
                'computed_affects' => array(
                    '__posts',
                ),
                'toggle_slug' => 'main_content',
            ),
            'template' => array(
                'label' => esc_html__('Template for posts', $this->textdomain),
                'description' => esc_html__('Template file to use to display a post', $this->textdomain),
                'type' => 'select',
                'option_category' => 'layout',
                'options' => $templates,
                'toggle_slug' => 'main_content',
                'computed_affects' => array(
                    '__posts',
                ),
            ),
            ), $fields);

        $fields['__posts']['computed_depends_on'][] = 'post_type';
        $fields['__posts']['computed_depends_on'][] = 'taxonomy';
        $fields['__posts']['computed_depends_on'][] = 'include_taxonomies';
        $fields['__posts']['computed_depends_on'][] = 'template';

        // hide categories
        $fields['include_categories']['toggle_slug'] = '';

        return $fields;
    }

    /**
     * Shortcode
     * 
     * @param array $atts
     * @param string $content
     * @param string $function_name
     */
    function shortcode_callback($atts, $content = null, $function_name) {
        
        global $post;

		// Stored current global post as variable so global $post variable can be restored
		// to its original state when et_pb_blog shortcode ends to avoid incorrect global $post
		// being used on the page (i.e. blog + shop module in backend builder)
		$post_cache = $post;

        /**
         * Cached $wp_filter so it can be restored at the end of the callback.
         * This is needed because this callback uses the_content filter / calls a function
         * which uses the_content filter. WordPress doesn't support nested filter
         */
        global $wp_filter;
        $wp_filter_cache = $wp_filter;

        $module_id = $this->shortcode_atts['module_id'];
        $module_class = $this->shortcode_atts['module_class'];
        $fullwidth = $this->shortcode_atts['fullwidth'];
        $posts_number = $this->shortcode_atts['posts_number'];
        $include_categories = $this->shortcode_atts['include_categories'];
        $meta_date = $this->shortcode_atts['meta_date'];
        $show_thumbnail = $this->shortcode_atts['show_thumbnail'];
        $show_content = $this->shortcode_atts['show_content'];
        $show_author = $this->shortcode_atts['show_author'];
        $show_date = $this->shortcode_atts['show_date'];
        $show_categories = $this->shortcode_atts['show_categories'];
        $show_comments = $this->shortcode_atts['show_comments'];
        $show_pagination = $this->shortcode_atts['show_pagination'];
        $background_layout = $this->shortcode_atts['background_layout'];
        $show_more = $this->shortcode_atts['show_more'];
        $offset_number = $this->shortcode_atts['offset_number'];
        $masonry_tile_background_color = $this->shortcode_atts['masonry_tile_background_color'];
        $overlay_icon_color = $this->shortcode_atts['overlay_icon_color'];
        $hover_overlay_color = $this->shortcode_atts['hover_overlay_color'];
        $hover_icon = $this->shortcode_atts['hover_icon'];
        $use_overlay = $this->shortcode_atts['use_overlay'];
        $header_level = $this->shortcode_atts['header_level'];

        $post_type = $this->shortcode_atts['post_type'];
        $taxonomy = $this->shortcode_atts['taxonomy'];
        $include_taxonomies = $this->shortcode_atts['include_taxonomies'];
        $template = $this->shortcode_atts['template'];

        global $paged;

        $module_class = ET_Builder_Element::add_module_order_class($module_class, $function_name);
        $video_background = $this->video_background();
        $parallax_image_background = $this->get_parallax_image_background();

        $container_is_closed = false;

        $processed_header_level = et_pb_process_header_level($header_level, 'h2');

        // some themes do not include these styles/scripts so we need to enqueue them in this module to support audio post format
        wp_enqueue_style('wp-mediaelement');
        wp_enqueue_script('wp-mediaelement');

        // include easyPieChart which is required for loading Blog module content via ajax correctly
        wp_enqueue_script('easypiechart');

        // include ET Shortcode scripts
        wp_enqueue_script('et-shortcodes-js');

        // remove all filters from WP audio shortcode to make sure current theme doesn't add any elements into audio module
        remove_all_filters('wp_audio_shortcode_library');
        remove_all_filters('wp_audio_shortcode');
        remove_all_filters('wp_audio_shortcode_class');

        if ('' !== $masonry_tile_background_color) {
            ET_Builder_Element::set_style($function_name, array(
                'selector' => '%%order_class%% .et_pb_blog_grid .et_pb_post',
                'declaration' => sprintf(
                    'background-color: %1$s;',
                    esc_html($masonry_tile_background_color)
                ),
            ));
        }

        if ('' !== $overlay_icon_color) {
            ET_Builder_Element::set_style($function_name, array(
                'selector' => '%%order_class%% .et_overlay:before',
                'declaration' => sprintf(
                    'color: %1$s !important;',
                    esc_html($overlay_icon_color)
                ),
            ));
        }

        if ('' !== $hover_overlay_color) {
            ET_Builder_Element::set_style($function_name, array(
                'selector' => '%%order_class%% .et_overlay',
                'declaration' => sprintf(
                    'background-color: %1$s;',
                    esc_html($hover_overlay_color)
                ),
            ));
        }

        if ('on' === $use_overlay) {
            $data_icon = '' !== $hover_icon ? sprintf(
                    ' data-icon="%1$s"',
                    esc_attr(et_pb_process_font_icon($hover_icon))
                ) : '';

            $overlay_output = sprintf(
                '<span class="et_overlay%1$s"%2$s></span>', ( '' !== $hover_icon ? ' et_pb_inline_icon' : ''), $data_icon
            );
        }

        $overlay_class = 'on' === $use_overlay ? ' et_pb_has_overlay' : '';

        if ('on' !== $fullwidth) {
            wp_enqueue_script('salvattore');

            $background_layout = 'light';
        }

        $args = array('posts_per_page' => (int) $posts_number);

        $et_paged = is_front_page() ? get_query_var('page') : get_query_var('paged');

        if (is_front_page()) {
            $paged = $et_paged;
        }

        if ('' !== $include_categories) {
            $args['cat'] = $include_categories;
        }

        if (!is_search()) {
            $args['paged'] = $et_paged;
        }

        if ('' !== $offset_number && !empty($offset_number)) {
            /**
             * Offset + pagination don't play well. Manual offset calculation required
             * @see: https://codex.wordpress.org/Making_Custom_Queries_using_Offset_and_Pagination
             */
            if ($paged > 1) {
                $args['offset'] = ( ( $et_paged - 1 ) * intval($posts_number) ) + intval($offset_number);
            }
            else {
                $args['offset'] = intval($offset_number);
            }
        }

        if (is_single() && !isset($args['post__not_in'])) {
            $args['post__not_in'] = array(get_the_ID());
        }

        // Images: Add CSS Filters and Mix Blend Mode rules (if set)
        if (array_key_exists('image', $this->advanced_options) && array_key_exists('css', $this->advanced_options['image'])) {
            $module_class .= $this->generate_css_filters(
                $function_name, 'child_', self::$data_utils->array_get($this->advanced_options['image']['css'], 'main', '%%order_class%%')
            );
        }
        
        $args['post_type'] = $post_type;

        if ('' !== $taxonomy && '' !== $include_taxonomies)
            $args['tax_query'] = array(
                array(
                    'taxonomy' => $taxonomy,
                    'field' => 'slug',
                    'terms' => $include_taxonomies,
                )
            );
        ob_start();

        query_posts($args);

        if (have_posts()) {
            if ('off' === $fullwidth) {
                echo '<div class="et_pb_salvattore_content" data-columns>';
            }

            while (have_posts()) {
                the_post();

                global $post;
                
                echo ws_view('modules/blog/' . $template, compact(
                    'overlay_class',
                    'fullwidth',
                    'show_thumbnail',
                    'post',
                    'use_overlay',
                    'overlay_output',
                    'processed_header_level',
                    'show_author',
                    'show_date',
                    'show_categories',
                    'show_comments',
                    'meta_date',
                    'show_content',
                    'show_more'
                ));
                
            } // endwhile

            if ('off' === $fullwidth) {
                echo '</div><!-- .et_pb_salvattore_content -->';
            }

            if ('on' === $show_pagination && !is_search()) {
                if (function_exists('wp_pagenavi')) {
                    wp_pagenavi();
                }
                else {
                    if (et_is_builder_plugin_active()) {
                        include( ET_BUILDER_PLUGIN_DIR . 'includes/navigation.php' );
                    }
                    else {
                        get_template_part('includes/navigation', 'index');
                    }
                }

                echo '</div> <!-- .et_pb_posts -->';

                $container_is_closed = true;
            }
        }
        else {
            if (et_is_builder_plugin_active()) {
                include( ET_BUILDER_PLUGIN_DIR . 'includes/no-results.php' );
            }
            else {
                get_template_part('includes/no-results', 'index');
            }
        }

        wp_reset_query();

        $posts = ob_get_contents();

        ob_end_clean();

        $class = " et_pb_bg_layout_{$background_layout}";

        if ( 'on' !== $fullwidth ) {
            $output = sprintf(
                '<div%5$s class="et_pb_module et_pb_blog_grid_wrapper%6$s">
                    <div class="%1$s%3$s%7$s%9$s%11$s">
                    %10$s
                    %8$s
                    <div class="et_pb_ajax_pagination_container">
                        %2$s
                    </div>
                    %4$s %12$s
                </div>',
                ( 'on' === $fullwidth ? 'et_pb_posts' : 'et_pb_blog_grid clearfix' ),
                $posts,
                esc_attr( $class ),
                ( ! $container_is_closed ? '</div> <!-- .et_pb_posts -->' : '' ),
                ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
                ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' ),
                '' !== $video_background ? ' et_pb_section_video et_pb_preload' : '',
                $video_background,
                '' !== $parallax_image_background ? ' et_pb_section_parallax' : '',
                $parallax_image_background,
                $this->get_text_orientation_classname(),
                $this->drop_shadow_back_compatibility( $function_name )
            );
        }
        else {
            $output = sprintf(
                '<div%5$s class="et_pb_module %1$s%3$s%6$s%7$s%9$s%11$s">
                %10$s
                %8$s
                <div class="et_pb_ajax_pagination_container">
                    %2$s
                </div>
                %4$s %12$s',
                ( 'on' === $fullwidth ? 'et_pb_posts' : 'et_pb_blog_grid clearfix' ),
                $posts,
                esc_attr( $class ),
                ( ! $container_is_closed ? '</div> <!-- .et_pb_posts -->' : '' ),
                ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
                ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' ),
                '' !== $video_background ? ' et_pb_section_video et_pb_preload' : '',
                $video_background,
                '' !== $parallax_image_background ? ' et_pb_section_parallax' : '',
                $parallax_image_background,
                $this->get_text_orientation_classname(),
                $this->drop_shadow_back_compatibility( $function_name )
            );
        }

        // Restore $wp_filter
        $wp_filter = $wp_filter_cache;
        unset($wp_filter_cache);

        // Restore global $post into its original state when et_pb_blog shortcode ends to avoid
        // the rest of the page uses incorrect global $post variable
        $post = $post_cache;

        return $output;
    }
    
    /**
     * Since the styling file is not updated until the author updates the page/post,
     * we should keep the drop shadow visible.
     *
     * @param string $functions_name
     *
     * @return string
     */
    private function drop_shadow_back_compatibility($functions_name) {
        $utils = ET_Core_Data_Utils::instance();
        $atts = $this->shortcode_atts;

        if (
            version_compare($utils->array_get($atts, '_builder_version', '3.0.93'), '3.0.94', 'lt') &&
            'on' !== $utils->array_get($atts, 'fullwidth') &&
            'on' === $utils->array_get($atts, 'use_dropshadow')
        ) {
            $class = self::get_module_order_class($functions_name);

            return sprintf(
                '<style>%1$s</style>', sprintf('.%1$s  article.et_pb_post { box-shadow: 0 1px 5px rgba(0,0,0,.1) }', esc_html($class))
            );
        }

        return '';
    }

}

new WS_Builder_Module_Blog();
